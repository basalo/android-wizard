##Introducción
Un wizard es una aplicación que nos guiará a través de una serie de pasos necesarios para recabar información o completar una tarea.

![Android Wizard App](http://blog.josebasalo.com/content/images/2015/Jul/asdffffff.gif)

Para comenzar creamos un nuevo proyecto con una activity en blanco. El layout principal, incorpora un textview para mostrar la información obtenida del asistente y un botón para ejecutarlo:

`activity_main.xml`
```
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:paddingLeft="@dimen/activity_horizontal_margin"
    android:paddingRight="@dimen/activity_horizontal_margin"
    android:paddingTop="@dimen/activity_vertical_margin"
    android:paddingBottom="@dimen/activity_vertical_margin"
    tools:context=".MainActivity">

    <Button
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Click Me"
        android:id="@+id/button"
        android:onClick="launchWizard"
        android:layout_alignParentTop="true"
        android:layout_centerHorizontal="true" />

    <TextView
        android:id="@+id/tv_info"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_below="@+id/button"
        android:layout_centerHorizontal="true"
        android:layout_marginTop="29dp" />

</RelativeLayout>
```

`MainActivity.java`
```
public class MainActivity extends AppCompatActivity {

	private TextView mDataView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
		mDataView = (TextView) findViewById(R.id.tv_info);

    }

	public void launchWizard(View v) {
        Intent i = new Intent(this, WizardActivity.class);
        startActivityForResult(i, KEY_WIZARD);
    }

}
```



##Wizard

Ahora creamos la activity y el layout que va a contener nuestro asistente, `WizardActivity.java` y `activity_wizard.xml`, esta activity será la encargada de mostrar los diferentes pasos, de almacenar los datos introducidos en cada uno de ellos y de devolver estos datos a la activity principal.

Para la creación de los pasos utilizamos un `ViewPager` el cual nos permite navegar entre los diferentes pasos (Fragments). Pero esta navegación queremos que sea controlada y ordenada, para ello utilizaremos dos botones, Anterior y Siguiente. 

El `ViewPager` permite hacer Swipe para cambiar de un fragment a otro, por tanto tenemos que desactivar esta característica. Para ello creamos una nueva clase con la cual desactivaremos ese gesto.

`NoSwipeViewPager.java`
```
public class NoSwipeViewPager extends ViewPager {

    private boolean enabled;

    public NoSwipeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.enabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }
``` 

Luego en layout del wizard en vez de utilizar un `ViewPager` como haríamos habitualmente utilizamos el que acabamos de definir:
```
<com.josebasalo.androidwizard.NoSwipeViewPager
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/step_container"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:layout_above="@+id/horizontal_line"
    android:layout_below="@+id/include_toolbar" />
```

Con lo que quedaría de la siguiente forma:

`activity_wizard.xml`
```
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="fill_parent"
    android:layout_height="fill_parent">

<View   android:id="@+id/horizontal_line"
    android:layout_width="match_parent"
    android:layout_height="1dp"
    android:background="@android:color/darker_gray"
    android:layout_above="@+id/wizard_button_bar"/>

<!-- Layout for wizard controls -->
<LinearLayout
    android:id="@+id/wizard_button_bar"
    android:orientation="horizontal"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_alignParentBottom="true"
    >
    <Button
        android:id="@+id/wizard_previous_button"
        android:text="@string/action_previous"
        android:enabled="false"
        android:layout_width="0dp"
        android:onClick="clickPrev"
        android:layout_height="wrap_content"
        android:layout_weight="1"
        style="?android:attr/borderlessButtonStyle"
        />
    <View
        android:layout_width="1dp"
        android:layout_height="match_parent"
        android:background="@android:color/darker_gray"
        />
    <Button
        android:id="@+id/wizard_next_button"
        android:text="@string/action_next"
        android:enabled="false"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_weight="1"
        android:onClick="clickNext"
        style="?android:attr/borderlessButtonStyle"
        />
</LinearLayout>

<com.josebasalo.androidwizard.NoSwipeViewPager
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/step_container"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:layout_above="@+id/horizontal_line" />

</RelativeLayout>
```
Y la activity para el wizard:

`WizardActivity.java`
```
public class WizardActivity extends AppCompatActivity {
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wizard);
    }
}
```

###Pasos del asistente

Cada paso del asistente es un fragment con su correspondiente layout, en este caso vamos a crear 3.

Para no repetir código común a todos los fragments vamos a crear una clase de la que hereden todos nuestros fragments, la cual luego vamos a modificar más adelante:

`FragmentStep.java`
```
public abstract class FragmentStep extends Fragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
    }

}
```

####Paso 1
`step_1.xml`
```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:paddingLeft="@dimen/activity_horizontal_margin"
    android:paddingRight="@dimen/activity_horizontal_margin"
    android:paddingTop="@dimen/activity_vertical_margin"
    android:paddingBottom="@dimen/activity_vertical_margin">

    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="18sp"
        android:text="@string/title_step_1"/>

    <EditText
        android:id="@+id/et_name"
        android:layout_width="match_parent"
        android:layout_height="wrap_content" />

</LinearLayout>
```

`FragmentStep1.java`
```
public class FragmentStep1 extends FragmentStep {
    private int page;

    private EditText mNameView;
    private String mName;

    // newInstance constructor for creating fragment with arguments
    public static FragmentStep1 newInstance(int page) {
        FragmentStep1 fragmentFirst = new FragmentStep1();
        Bundle args = new Bundle();
        args.putInt(WizardActivity.KEY_STEP_PAGE, page);
        fragmentFirst.setArguments(args);
        fragmentFirst.setRetainInstance(true);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt(WizardActivity.KEY_STEP_PAGE, 0);
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.step_1, container, false);

        mNameView = (EditText) v.findViewById(R.id.et_name);
        mNameView.requestFocus();

        return v;
    }
}
```

Como se puede observar en el código aparece la variable `page` la cual no se usa en ningún sitio, está ahí a modo de ejemplo, de como crear los fragments con argumentos.

####Paso 2
`step_2.xml`
```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:paddingLeft="@dimen/activity_horizontal_margin"
    android:paddingRight="@dimen/activity_horizontal_margin"
    android:paddingTop="@dimen/activity_vertical_margin"
    android:paddingBottom="@dimen/activity_vertical_margin">

    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="18sp"
        android:text="@string/title_step_2"/>

    <RadioGroup
        android:id="@+id/rg_gender"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:checkedButton="@+id/rb_male">

        <RadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/male"
            android:id="@+id/rb_male" />

        <RadioButton
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/female"
            android:id="@+id/rb_female" />

    </RadioGroup>
    
</LinearLayout>
```

`FragmentStep2.java`
```
public class FragmentStep2 extends FragmentStep {
    private int page;

    private RadioGroup mGenderView;
    private String mGender;

    public static FragmentStep2 newInstance(int page) {
        FragmentStep2 fragmentSecond = new FragmentStep2();
        Bundle args = new Bundle();
        args.putInt(WizardActivity.KEY_STEP_PAGE, page);
        fragmentSecond.setArguments(args);
        fragmentSecond.setRetainInstance(true);
        return fragmentSecond;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt(WizardActivity.KEY_STEP_PAGE, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.step_2, container, false);
        mGenderView = (RadioGroup) v.findViewById(R.id.rg_gender);

        return v;
    }
}
```

####Paso 3
Este paso no realiza ninguna tarea, es un paso en blanco al que se llama al finalizar el asistente para obtener los datos del paso previo (el último útil).

`FragmentStep3.java`
```
//Foo fragment
public class FragmentStep3 extends FragmentStep {

    public static FragmentStep3 newInstance(int page) {
        FragmentStep3 fragmentThree = new FragmentStep3();
        fragmentThree.setRetainInstance(true);
        return fragmentThree;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return new LinearLayout(getActivity());
    }
}
```

####Definir el adaptador para el ViewPager
Ahora es el momento de definir el adaptador para el `ViewPager`.

`PagerAdapter.java`
```
public class PagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FragmentStep1.newInstance(0);
            case 1:
                return FragmentStep2.newInstance(1);
			case 2:
                return FragmentStep3.newInstance(2);
            default:
                return null;
        }
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
```

y a continuación inicializamos el `ViewPager` en `WizardActivity.java`.
```
public class WizardActivity extends BaseAppCompatActivity {

	//..
	private FragmentPagerAdapter adapterViewPager;
	private NoSwipeViewPager vpPager;
	//..

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewPager();
    }

	 private void initViewPager() {
        vpPager = (NoSwipeViewPager) findViewById(R.id.step_container);
        adapterViewPager = new PagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        //Disable swipe pages
        vpPager.setPagingEnabled(false);
        //number of pages that should be retained to either side of the current page in the view hierarchy in an idle state
        vpPager.setOffscreenPageLimit(2);

        vpPager.setCurrentItem(0);
    }

}
```

###Definir la navegación entre los pasos
Para navegar entre los diferentes pasos utilizaremos los botones de Anterior y Siguiente que hemos definido, para ello debemos implementar los métodos que especificamos en el `WizardLayout.xml` para capturar los clicks (clickNext y clickPrev).

```
public class WizardActivity extends BaseAppCompatActivity {
	//..
	private Button mButtonNext;
    private Button mButtonPrev;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       	//..

        mButtonNext = (Button) findViewById(R.id.wizard_next_button);
        mButtonPrev = (Button) findViewById(R.id.wizard_previous_button);

    }
	public void clickNext(View view) {
        switch (vpPager.getCurrentItem()) {
			case 0:
                vpPager.setCurrentItem(1);
                mButtonPrev.setEnabled(true);
                mButtonNext.setText(getString(R.string.action_finalize));
                break;

            case 1:

                break;
        }
    }

    public void clickPrev(View view) {
        switch (vpPager.getCurrentItem()) {
			case 1:
                vpPager.setCurrentItem(0);
                mButtonPrev.setEnabled(false);
                mButtonNext.setText(getString(R.string.action_next));
                break;
        }
    }
}
```
Como se obseva en el cógio, al hacer click en uno de los botones recuperamos el paso en el que estamos para así decidir cual es el siguiente, el texto que tendrán los botones, cuales estarán habilitados y cuales no.

Por defecto en el paso 1 queremos que el usuario introduzca el nombre antes de poder continuar (es un paso obligatorio), por ello no se activará el botón de siguiente hasta que se haya completado este paso.

###Notificar cuando un paso está completo o incompleto

Para notificar que un paso está completado implementaremos unha interfaz que usarán los pasos para notificar al asistente su estado y así permitir o no avanzar hacia el siguiente paso.

Primero definimos la interfaz `StepCompletedListener`:
```
public interface StepCompletedListener {
    void stepCompleted(boolean complete);
} 
```

Como queremos que todos los pasos puedan notificar su estado, definimos los métodos del listener en la clase abstracta `FragmentStep.java`:
```
public abstract class FragmentStep extends Fragment {

    private StepCompletedListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (StepCompletedListener) activity;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
        }
    }

    void notifyStateComplete() {
        listener.stepCompleted(true);
    }

    void notifyStateIncomplete() {
        listener.stepCompleted(false);
    }
}
```
Y por último hacemos que el wizard implemente el listener:
```
public class WizardActivity extends AppCompatActivity
        implements StepCompletedListener {

	//..

	@Override
    public void stepCompleted(boolean complete) {
        if (complete)
            mButtonNext.setEnabled(true);
        else
            mButtonNext.setEnabled(false);
    }

}
```

Una vez hecho esto debemos especificar cuando un paso está completo o no. En el caso del nombre, estará incompleto hasta que el usuario haya introducido al menos un caracter.

Para detectar esto añadiremos un `TextWatcher()`:

```
public class FragmentStep1 extends FragmentStep {
	//..
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

		//..
		mNameView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mName = s.toString();
                    notifyStateComplete();
                } else
                    notifyStateIncomplete();
            }
        });

}
```

###Recuperar los datos de cada step

Una vez definida la navegación entre los pasos, tenemos que recuperar los valores introducidos por el usuario. Para ello definiremos otra interfaz la cual será utilizada por los pasos para notificar al wizard los valores.

```
public interface SetParameterListener {
    void setParameter(String key, String value);
}
```

Al igual que con el anterior listener, tenemos que inicializarlo y definir en el `FragmentStep.java` el método que utilizarán los pasos quedando de la siguiente manera:
```
public abstract class FragmentStep extends Fragment {

    private StepCompletedListener listener;
    private SetParameterListener setListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (StepCompletedListener) activity;
            setListener = (SetParameterListener) activity;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
        }
    }

    public void notifyStateComplete() {
        listener.stepCompleted(true);
    }

    public void notifyStateIncomplete() {
        listener.stepCompleted(false);
    }

    public void setParameter(String key, String value) {
        setListener.setParameter(key, value);
    }
}
```

Y por supuesto el wizard deberá implementar ese listener:
```
public class WizardActivity extends AppCompatActivity
        implements StepCompletedListener, SetParameterListener {
	//..
	@Override
    public void setParameter(String key, String value) {

    }
}
```

En esta aplicación de ejemplo utilizamos simplemente dos campos, nombre y género, pero no será lo habitual en el día a día. Por ello vamos a definir una entidad `StepsParameters.java` que recoja todos los valores introducidos por el usuario.

Así mismo, para simplificar el paso de parámetros entre activities vamos a utilizar la librería [Parceler](https://github.com/johncarl81/parceler) para implementar StepsParameters como parcelable. Para ello primero añadimos la dependencia:
```
compile "org.parceler:parceler-api:1.0.1"
provided "org.parceler:parceler:1.0.1"
```

Y creamos la clase `StepsParameters.java`:
```

@Parcel
public class StepsParameters {

    String name;
    String genre;

    public StepsParameters() {}
    
    public StepsParameters(String genre, String name) {
        this.genre = genre;
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
```

Ahora es necesario definir los TAG que se emplearán para pasar los valores entre los pasos y el wizard,
```
public class WizardActivity extends AppCompatActivity
        implements StepCompletedListener, SetParameterListener {

    public static final String KEY_STEP_PAGE = "step_page";
    
    public static final String TAG_NAME = "name";
    public static final String TAG_GENRE = "genre";
    public static final String VALUE_MALE = "male";
    public static final String VALUE_FEMALE = "female";

	//..
}
```

inicializar el objeto que almacenará los valores en el onCreate,
```
stepsParameters = new StepsParameters();
```

y guardar los datos enviados por los pasos:
```
public class WizardActivity extends AppCompatActivity
        implements StepCompletedListener, SetParameterListener {

	//..
	@Override
    public void setParameter(String key, String value) {
        if (key.equals(TAG_NAME))
            stepsParameters.setName(value);
        else if (key.equals(TAG_GENRE))
            stepsParameters.setGenre(value);
    }
}
```

Pero, ¿Cuándo enviamos estos valores desde los pasos al wizard? Pues antes de que el paso deje de estar visible para el usuario (simplificando, ya que el ciclo de vida de los fragments es algo bastante complejo). Esto lo podemos hacer en el método `setUserVisibleHint`:
```
	@Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
                //Save value in activity
                if (!TextUtils.isEmpty(mName)) {
                    setParameter(WizardActivity.TAG_NAME, mName);
                }
            } else {
                //Active next button if name is not empty and back from incomplete state
                if (!TextUtils.isEmpty(mNameView.getText().toString()))
                    notifyStateComplete();
				else
                    notifyStateIncomplete();
            }
        }
    }
```

Para finalizar nuestro wizard, debemos devolver los datos recogidos a la activity principal:
```
public void clickNext(View view) {
	//..
		case 1:
		//Return to MainActivity with result ok
                vpPager.setCurrentItem(2);

                Intent i = new Intent(this, MainActivity.class);
                i.putExtra(TAG_DATA, Parcels.wrap(stepsParameters));
                setResult(RESULT_OK, i);
                finish();
                break;
}
```


##Utilizar los datos devueltos por el wizard
En la activity principal debemos recoger el resultado de la ejecución del wizard:
```
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == KEY_WIZARD) {

            if (resultCode == RESULT_OK) {

                StepsParameters parameters = (StepsParameters) Parcels.unwrap(
                        (android.os.Parcelable) data.getExtras().get(
                                WizardActivity.TAG_DATA));

                mDataView.setText(parameters.toString());

            }
        }
    }
```

##Conclusión
Con esta estructura base, podemos hacer cosas mucho más complejas, añadiendo más pasos y/o añadiendo más campos por paso, así como otro tipo de elementos, check boxes, listas, etc.
