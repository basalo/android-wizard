package com.josebasalo.androidwizard.wizard.steps;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.josebasalo.androidwizard.wizard.listeners.SetParameterListener;
import com.josebasalo.androidwizard.wizard.listeners.StepCompletedListener;

public abstract class FragmentStep extends Fragment {

    private StepCompletedListener listener;
    private SetParameterListener setListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (StepCompletedListener) activity;
            setListener = (SetParameterListener) activity;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
        }
    }

    void notifyStateComplete() {
        listener.stepCompleted(true);
    }

    void notifyStateIncomplete() {
        listener.stepCompleted(false);
    }

    void setParameter(String key, String value) {
        setListener.setParameter(key, value);
    }
}
