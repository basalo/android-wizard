package com.josebasalo.androidwizard.wizard.steps;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

//Foo fragment
public class FragmentStep3 extends FragmentStep {

    public static FragmentStep3 newInstance(int page) {
        FragmentStep3 fragmentThree = new FragmentStep3();
        fragmentThree.setRetainInstance(true);
        return fragmentThree;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return new LinearLayout(getActivity());
    }
}
