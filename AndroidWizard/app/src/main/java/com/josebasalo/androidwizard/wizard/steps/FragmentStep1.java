package com.josebasalo.androidwizard.wizard.steps;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.josebasalo.androidwizard.R;
import com.josebasalo.androidwizard.wizard.WizardActivity;

public class FragmentStep1 extends FragmentStep {

    private EditText mNameView;
    private String mName;

    // newInstance constructor for creating fragment with arguments
    public static FragmentStep1 newInstance(int page) {
        FragmentStep1 fragmentFirst = new FragmentStep1();
        Bundle args = new Bundle();
        args.putInt(WizardActivity.KEY_STEP_PAGE, 0);
        fragmentFirst.setArguments(args);
        fragmentFirst.setRetainInstance(true);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int page = getArguments().getInt(WizardActivity.KEY_STEP_PAGE, 0);
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.step_1, container, false);

        mNameView = (EditText) v.findViewById(R.id.et_name);
        mNameView.requestFocus();

        mNameView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mName = s.toString();
                    notifyStateComplete();
                } else
                    notifyStateIncomplete();
            }
        });

        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
                //Save value in activity
                if (!TextUtils.isEmpty(mName)) {
                    setParameter(WizardActivity.TAG_NAME, mName);
                }
            } else {
                //Active next button if name is not empty and back from incomplete state
                if (!TextUtils.isEmpty(mNameView.getText().toString()))
                    notifyStateComplete();
                else
                    notifyStateIncomplete();
            }
        }
    }
}
