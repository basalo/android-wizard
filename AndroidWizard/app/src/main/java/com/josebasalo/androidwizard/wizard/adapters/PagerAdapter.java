package com.josebasalo.androidwizard.wizard.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.josebasalo.androidwizard.wizard.steps.FragmentStep1;
import com.josebasalo.androidwizard.wizard.steps.FragmentStep2;
import com.josebasalo.androidwizard.wizard.steps.FragmentStep3;

public class PagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FragmentStep1.newInstance(0);
            case 1:
                return FragmentStep2.newInstance(1);
            case 2:
                return FragmentStep3.newInstance(2);
            default:
                return null;
        }
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
