package com.josebasalo.androidwizard.wizard.listeners;

public interface StepCompletedListener {
    void stepCompleted(boolean complete);
}
