package com.josebasalo.androidwizard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.josebasalo.androidwizard.wizard.WizardActivity;
import com.josebasalo.androidwizard.wizard.models.StepsParameters;

import org.parceler.Parcels;


public class MainActivity extends AppCompatActivity {

    private static final int KEY_WIZARD = 0;
    private TextView mDataView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mDataView = (TextView) findViewById(R.id.tv_info);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == KEY_WIZARD) {

            if (resultCode == RESULT_OK) {

                StepsParameters parameters = (StepsParameters) Parcels.unwrap(
                        (android.os.Parcelable) data.getExtras().get(
                                WizardActivity.TAG_DATA));

                mDataView.setText(parameters.toString());

            }
        }
    }

    public void launchWizard(View v) {
        Intent i = new Intent(this, WizardActivity.class);
        startActivityForResult(i, KEY_WIZARD);
    }
}
