package com.josebasalo.androidwizard.wizard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.josebasalo.androidwizard.MainActivity;
import com.josebasalo.androidwizard.R;
import com.josebasalo.androidwizard.wizard.adapters.PagerAdapter;
import com.josebasalo.androidwizard.wizard.listeners.SetParameterListener;
import com.josebasalo.androidwizard.wizard.listeners.StepCompletedListener;
import com.josebasalo.androidwizard.wizard.models.StepsParameters;

import org.parceler.Parcels;


public class WizardActivity extends AppCompatActivity
        implements StepCompletedListener, SetParameterListener {

    public static final String KEY_STEP_PAGE = "step_page";

    public static final String TAG_NAME = "name";
    public static final String TAG_GENRE = "genre";
    public static final String VALUE_MALE = "male";
    public static final String VALUE_FEMALE = "female";

    public static final String TAG_DATA = "data";

    private FragmentPagerAdapter adapterViewPager;
    private NoSwipeViewPager vpPager;

    private Button mButtonNext;
    private Button mButtonPrev;

    private StepsParameters stepsParameters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wizard);

        initViewPager();

        mButtonNext = (Button) findViewById(R.id.wizard_next_button);
        mButtonPrev = (Button) findViewById(R.id.wizard_previous_button);

        stepsParameters = new StepsParameters();

    }

    private void initViewPager() {
        vpPager = (NoSwipeViewPager) findViewById(R.id.step_container);
        adapterViewPager = new PagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        //Disable swipe pages
        vpPager.setPagingEnabled(false);
        //number of pages that should be retained to either side of the current page in the view hierarchy in an idle state
        vpPager.setOffscreenPageLimit(2);

        vpPager.setCurrentItem(0);
    }

    public void clickNext(View view) {
        switch (vpPager.getCurrentItem()) {
            case 0:
                vpPager.setCurrentItem(1);
                mButtonPrev.setEnabled(true);
                mButtonNext.setText(getString(R.string.action_finalize));
                break;

            case 1:
                //Return to MainActivity with result ok
                vpPager.setCurrentItem(2);

                Intent i = new Intent(this, MainActivity.class);
                i.putExtra(TAG_DATA, Parcels.wrap(stepsParameters));
                setResult(RESULT_OK, i);
                finish();
                break;
        }
    }

    public void clickPrev(View view) {
        switch (vpPager.getCurrentItem()) {
            case 1:
                vpPager.setCurrentItem(0);
                mButtonPrev.setEnabled(false);
                mButtonNext.setText(getString(R.string.action_next));
                break;
        }
    }

    @Override
    public void stepCompleted(boolean complete) {
        if (complete)
            mButtonNext.setEnabled(true);
        else
            mButtonNext.setEnabled(false);
    }


    @Override
    public void setParameter(String key, String value) {
        if (key.equals(TAG_NAME))
            stepsParameters.setName(value);
        else if (key.equals(TAG_GENRE))
            stepsParameters.setGenre(value);
    }
}
