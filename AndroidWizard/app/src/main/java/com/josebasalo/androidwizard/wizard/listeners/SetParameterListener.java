package com.josebasalo.androidwizard.wizard.listeners;

public interface SetParameterListener {
    void setParameter(String key, String value);
}
