package com.josebasalo.androidwizard.wizard.models;

import org.parceler.Parcel;

@Parcel
public class StepsParameters {

    String name;
    String genre;

    public StepsParameters() {}

    public StepsParameters(String genre, String name) {
        this.genre = genre;
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "StepsParameters{" +
                "genre='" + genre + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
