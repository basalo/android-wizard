package com.josebasalo.androidwizard.wizard.steps;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.josebasalo.androidwizard.R;
import com.josebasalo.androidwizard.wizard.WizardActivity;

public class FragmentStep2 extends FragmentStep {

    private RadioGroup mGenderView;

    public static FragmentStep2 newInstance(int page) {
        FragmentStep2 fragmentSecond = new FragmentStep2();
        Bundle args = new Bundle();
        args.putInt(WizardActivity.KEY_STEP_PAGE, page);
        fragmentSecond.setArguments(args);
        fragmentSecond.setRetainInstance(true);
        return fragmentSecond;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int page = getArguments().getInt(WizardActivity.KEY_STEP_PAGE, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.step_2, container, false);
        mGenderView = (RadioGroup) v.findViewById(R.id.rg_gender);

        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
                //Save value in activity
                String value = WizardActivity.VALUE_MALE;
                if (mGenderView.getCheckedRadioButtonId() == R.id.rb_female)
                    value = WizardActivity.VALUE_FEMALE;

                setParameter(WizardActivity.TAG_GENRE, value);
            }
        }
    }
}
